/**
 * @fileoverview Task two implementation
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React from 'react'
import styled from 'styled-components'
import './styles.css'

//------------------------------------------------------------------------------
// Styled components
//------------------------------------------------------------------------------

const BigH1 = styled.h1`
  font-size: 15rem;
  color: rgb(1, 127, 250);
  font-weight: 900;
`

const BannerHead = styled.h1`
  font-size: 4.5rem;
  font-weight: 800;
  text-transform: uppercase;
  width: 340px;
  text-align: left;
  margin: auto;
  margin-top: -3rem;
  line-height: 1;
`

const BannerTextWrapper = styled.div`
  padding: 1rem 5rem;
  width: 60%;
  margin: auto;
  margin-top: 4rem;
  text-transform: uppercase;
  color: #2c2c2c;
`

const BannerTextBlackLine = styled.span`
  display: inline-block;
  width: 300px;
  height: 10px;
  vertical-align: text-top;
  background: #2c2c2c;
`

const BannerText = styled.p`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  font-size: ${({ fs }) => fs || '1.5rem'};
  font-weight: ${({ fw }) => fw || '700'};
  letter-spacing: ${({ ls }) => ls || '-1px'};
`

const Square = styled.div`
  width: ${({ siz }) => siz || '150px'};
  height: ${({ siz }) => siz || '150px'};
  background: ${({ bg }) => bg || '#dc403b'};

  &:after {
    content: '';
    position: absolute;
    top: ${({ innerGap }) => innerGap || '25px'};
    left: ${({ innerGap }) => innerGap || '25px'};
    width: ${({ offset }) => offset || '100px'};
    height: ${({ offset }) => offset || '100px'};
    background-color: rgb(244, 245, 246);
    z-index: 2;
  }
  transform: ${({ deg }) => deg || 'rotate(45deg)'};
  margin: 0 auto;
  position: absolute;
  top: ${({ top }) => top || 'unset'};
  left: ${({ left }) => left || 'unset'};
  bottom: ${({ bottom }) => bottom || 'unset'};
  right: ${({ right }) => right || 'unset'};
  margin-top: -100px;
  margin-left: -100px;
`
const Circle = styled(Square)`
  border-radius: 50%;
  filter: blur(5px);
  &: after {
    border-radius: 50%;
  }
`

//------------------------------------------------------------------------------
// Public Components
//------------------------------------------------------------------------------

/**
 * task two component
 */
export default class Task2 extends React.Component {
  render() {
    return (
      <div className='App' style={{ margin: '5rem auto' }}>
        <h1>Devfolio Task 2</h1>

        <BigH1>30</BigH1>
        <Square
          siz='23px'
          bg='#0366fa'
          innerGap='7px'
          offset='8px'
          top='59%'
          left='39%'
        />

        <Square
          siz='20px'
          bg='#f97398'
          innerGap='7px'
          deg='rotate(0deg)'
          offset='0px'
          top='57%'
          right='33%'
        />

        <Square
          siz='23px'
          bg='#f97398'
          deg='rotate(0deg)'
          innerGap='7px'
          offset='8px'
          top='67%'
          right='23%'
        />

        <Square
          deg='rotate(86deg)'
          siz='50px'
          innerGap='13px'
          offset='25px'
          top='80%'
          left='33%'
        />

        <Square
          siz='10px'
          bg='#0467fa'
          innerGap='7px'
          deg='rotate(-42deg)'
          offset='0px'
          top='93%'
          right='32%'
        />
        <Circle
          siz='73px'
          bg='#f5b62c'
          innerGap='12px'
          offset='50px'
          top='100%'
          left='71%'
        />
        <BannerHead> Hours of ...</BannerHead>
        <BannerTextWrapper>
          <BannerText>
            {'designing / building / coding / hacking'
              .split('/')
              .map((t, i) => (
                <span>
                  {t}
                  {i === 3 ? '' : <span>{'\t'} /</span>}
                </span>
              ))}
          </BannerText>
          <BannerText fs='1.2rem'>
            {'networking / friends / mentors / competitions'
              .split('/')
              .map((t, i) => (
                <span>
                  {t} {i === 3 ? '' : <span> /</span>}
                </span>
              ))}
          </BannerText>
          <BannerText fs='1rem' ls='-1px'>
            {'coffee / tea / green tea / food / swags / t-shirts'
              .split('/')
              .map((t, i) => (
                <span>
                  {t} {i === 5 ? '' : <span> /</span>}
                </span>
              ))}
          </BannerText>
          <BannerText fs='.8rem' ls='-0.5px'>
            {'super fast internet / talks / did we mention green tea ? / brand new  APIs'
              .split('/')
              .map((t, i) => (
                <span>
                  {t} {i === 3 ? '' : <span> /</span>}
                </span>
              ))}
          </BannerText>
          <BannerText fs='.6rem' ls='-0.5px'>
            {' and / a / whole / lot / more '.split('/').map((t, i) => (
              <span>
                {t} {i === 4 ? '' : <span> /</span>}
              </span>
            ))}{' '}
            / <BannerTextBlackLine />
          </BannerText>
        </BannerTextWrapper>
      </div>
    )
  }
}
