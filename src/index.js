/**
 * @fileoverview Renderer
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

//------------------------------------------------------------------------------
// Execution
//------------------------------------------------------------------------------

const rootElement = document.getElementById('root')
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  rootElement
)
