/**
 * @fileoverview Main entry point.
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React, { Suspense } from 'react'
import './styles.css'
import styled from 'styled-components'
import BarLoader from 'react-spinners/BarLoader'
import ListApp from './task1/index.jsx'
import Task2 from './task2/index.jsx'

const LasyTask2 = React.lazy(() => import('./task2/index.jsx'))

//-----------------------------------------------------------------------------
// Styled Components
//-----------------------------------------------------------------------------

const LoaderWrap = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;

  div {
    width: 100%;
    margin: auto;
  }
`

//------------------------------------------------------------------------------
// Public Components
//------------------------------------------------------------------------------

/**
 * The App component which will be rendered at `app` id in `index.js`
 */
export default class App extends React.Component {
  render() {
    return (
      <div className='App'>
        <Suspense
          fallback={
            <LoaderWrap>
              <BarLoader color={'#017ffa'} />
            </LoaderWrap>
          }
        >
          <LasyTask2 />
          <h1>Devfolio Task 1</h1>
          <ListApp />
        </Suspense>
      </div>
    )
  }
}
