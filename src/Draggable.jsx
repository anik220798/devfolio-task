/**
 * @fileoverview Implementation of drag wrapper for independent implementation
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React from 'react'

//------------------------------------------------------------------------------
// Public Components
//------------------------------------------------------------------------------

/**
 * Wrapper for drag behavior
 */
export default class Draggable extends React.Component {
  /**
   * handler for onDragStart
   * params {*} e
   */
  drag = (e) => {
    e.dataTransfer.setData('transfer', e.target.id)
  }

  render() {
    return (
      <div
        id={this.props.id}
        draggable
        onDragStart={this.drag}
        onDragOver={(e) => e.stopPropagation()}
      >
        {this.props.children}
      </div>
    )
  }
}
