/**
 * @fileoverview Implementation of drag-n-drop list
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React from 'react'
import styled from 'styled-components'
import './implementationTwo.css'
import BarLoader from 'react-spinners/BarLoader'
import BounceLoader from 'react-spinners/BounceLoader'
import axios from 'axios'
import { AiOutlineCloseCircle } from 'react-icons/ai'
import { withFirebase } from '../firebase/FirebaseWrapper'
import { SUCCESS } from '../firebase/data'
//------------------------------------------------------------------------------
// Types
//------------------------------------------------------------------------------

/**
 *
 * @typedef DragI
 * @property {string} id
 *
 * @typedef DropI
 * @property {string} id
 *
 * @typedef {Object} ListAppI
 * @property {() : Promise<Array<string>>} getTasks
 * @property {(string) : Promise<Object>} addTask
 *
 */

//------------------------------------------------------------------------------
// Styled Components
//------------------------------------------------------------------------------

const Item = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  padding: 0px 16px;
`

const OptionList = styled.ul`
  position: absolute;
  width: 100%;
  border-radius: 4px;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  min-height: 56px;
  background: rgb(255, 255, 255);
  border-width: 1px;
  border-style: solid;
  border-color: rgb(186, 205, 247);
  padding: 4px 0px;
  margin: auto;
  top: 53px;
  list-style: none;
  border-top: none;
  width: 100%;

  li {
    padding: 0.5rem 1rem;
    width: 100%;
    margin-bottom: 4px;
    cursor: pointer;
    display: flex;

    &:hover {
      background: #84abf8;
    }
  }
`
const LoaderWrapper = styled.div`
  position: fixed;
  top: 0;
  width: 100%;
  left: 0;
`

const ListLoaderWrapper = styled.div`
  position: absolute;
  width: 100%;
  left: 0;

  div {
    margin: auto;
  }
`

const CloseComponent = styled(AiOutlineCloseCircle)`
  &:hover {
    opacity: 0.5;
  }
`

//------------------------------------------------------------------------------
// Public Components
//------------------------------------------------------------------------------

/**
 *
 * Wrapper for drop behavior
 * @param {DropI} props
 */
export function Drop(props) {
  const drop = (e) => {
    e.preventDefault()
    const id = e.dataTransfer.getData('item_id')

    const card = document.getElementById(id)
    card.style.display = 'block'
    e.target.appendChild(card)
  }

  const dragOver = (e) => {
    e.preventDefault()
  }
  return (
    <div className='drop' id={props.id} onDrop={drop} onDragOver={dragOver}>
      {props.children}
    </div>
  )
}

/**
 * Wrapper for drag behavior.
 * @param {DragI} props
 */
export function Drag(props) {
  /**
   * handler for onDragStart event
   * @param {*} e
   */
  const dragStart = (e) => {
    const target = e.target
    e.dataTransfer.setData('item_id', target.id)
    e.dataTransfer.effectAllowed = 'move'

    setTimeout(() => {
      target.style.display = 'none'
    }, 0)
  }

  /**
   * handler for onDragOver event
   * @param {*} e
   */
  const dragOver = (e) => {
    e.stopPropagation()
  }

  return (
    <div
      className='drag'
      id={props.id}
      draggable='true'
      onDragStart={dragStart}
      onDragOver={dragOver}
    >
      {props.children}
    </div>
  )
}

/**
 * List component having the list contents and renders the Drag and Drop components and also firebase wrapper
 */
class ListApp extends React.Component {
  /**
   * ListApp component .
   * @param {ListAppI} props
   */
  constructor(props) {
    super(props)
    this.state = {
      items: [],
      searchedTagsResults: [],
      isSearching: false,
      isListReady: false,
    }
    this.addTaskHandler = this.addTaskHandler.bind(this)
    this.searchTaskTags = this.searchTaskTags.bind(this)
    this.clearTagsResult = this.clearTagsResult.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)
    this.setWrapperRef = this.setWrapperRef.bind(this)
    this.handleRemoveTags = this.handleRemoveTags.bind(this)
    this.updateTasks = this.updateTasks.bind(this)
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside)
    this.updateTasks()
  }

  updateTasks() {
    this.props.getTasks().then((tasks) => {
      this.setState({
        items: tasks,
        isListReady: true,
      })
    })
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  /**
   * save the ref from triggered component
   * @param {*} node
   */
  setWrapperRef(node) {
    this.wrapperRef = node
  }

  /**
   * handler for clicking outside of wrapperRef component
   * @param {*} event
   */
  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.clearTagsResult()
    }
  }

  /**
   * Handler for adding task by capping this.props.addTask from HOC
   * @param {*} e
   */
  addTaskHandler(e) {
    this.props
      .addTask(e.target.innerText)
      .then(async (res) => {
        this.props.getTasks().then((tasks) => {
          this.setState({
            items: tasks,
          })
        })
      })
      .catch((err) => console.log('ERROR', err))
  }

  /**
   * Handler clearing state and indicating no searching is happening currently
   */
  clearTagsResult() {
    this.setState({
      searchedTagsResults: [],
      isSearching: false,
    })
  }

  /**
   * handler for searching for tags from stackexchange api and rendering the result of top 5 in list
   * @param {*} e
   */
  searchTaskTags(e) {
    this.setState({
      isSearching: true,
    })
    if (
      e.target.value === '' ||
      e.target.value === null ||
      e.target.value === undefined
    ) {
      this.setState({
        searchedTagsResults: [],
      })
      return
    } else {
      let currentTags = []
      axios
        .get(
          `https://api.stackexchange.com/2.2/tags/${e.target.value}/related?site=stackoverflow`
        )
        .then((res) => {
          res.data.items.slice(0, 5).forEach(async (item) => {
            await currentTags.push(item.name)
          })
          this.setState({
            searchedTagsResults: currentTags,
            isSearching: false,
          })
        })
    }
  }

  async handleRemoveTags(e, itemToDelete) {
    this.setState({
      isSearching: true,
    })
    const res = await this.props.deleteTask(itemToDelete)
    if (res.status && res.status === SUCCESS) {
      await this.updateTasks()
    } else {
      console.log('res.error :>> ', res.error)
    }
    this.setState({
      isSearching: false,
    })
  }

  render() {
    return (
      <div className='flexbox'>
        {this.state.isListReady ? (
          <>
            <Drop id='drop-1'>
              {this.state.items
                .slice(0, this.state.items.length / 2 + 1)
                .map((item, i) => {
                  return (
                    <Drag id={`item-${i}`}>
                      <Item>
                        <p>{item.name}</p>
                        <CloseComponent
                          onClick={(e) => this.handleRemoveTags(e, item.ref)}
                        />
                      </Item>
                    </Drag>
                  )
                })}
            </Drop>
            <Drop id='drop-1'>
              {this.state.items
                .slice(this.state.items.length / 2 + 1)
                .map((item, i) => {
                  return (
                    <Drag id={`item-${i}`}>
                      <Item>
                        <p>{item.name}</p>
                        <CloseComponent
                          onClick={(e) => this.handleRemoveTags(e, item.ref)}
                        />
                      </Item>
                    </Drag>
                  )
                })}
              <div className='addTaskBtn' ref={this.setWrapperRef}>
                <input
                  placeholder='Add Task'
                  type='text'
                  onKeyDown={this.clearTagsResult}
                  onInput={this.searchTaskTags}
                  onMouseDown={this.searchTaskTags}
                />
                <LoaderWrapper>
                  <BarLoader
                    css={{ width: '100%' }}
                    size={'100%'}
                    color={'rgb(71, 133, 255)'}
                    loading={this.state.isSearching}
                  />
                </LoaderWrapper>

                {this.state.searchedTagsResults.length !== 0 ? (
                  <OptionList>
                    {this.state.searchedTagsResults.map((s) => {
                      return <li onClick={this.addTaskHandler}>{s}</li>
                    })}
                  </OptionList>
                ) : null}
              </div>
            </Drop>
          </>
        ) : (
          <ListLoaderWrapper>
            <BounceLoader color={'#017ffa'} size={150} />
          </ListLoaderWrapper>
        )}
      </div>
    )
  }
}

export default withFirebase(ListApp)
