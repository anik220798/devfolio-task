/**
 * @fileoverview First implementation of drag-n-drop list
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React from 'react'
import './styles.css'

//------------------------------------------------------------------------------
// Public Components
//------------------------------------------------------------------------------

/**
 * List component having the list contents
 */
export default class ListApp extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      items: ['one', 'two', 'three', 'four', 'five', 'six'],
    }
    this.handleDragOver = this.handleDragOver.bind(this)
    this.handleDrop = this.handleDrop.bind(this)
    this.handleDragStart = this.handleDragStart.bind(this)
    this.handleDragEnd = this.handleDragEnd.bind(this)
  }

  /**
   *  handler for onDragOver
   * @param {*} e
   * @param {Number} index
   */
  handleDragOver(e, index) {
    // e.preventDefault();

    const draggedOverItem = this.state.items[index]

    // if the item is dragged over itself, ignore
    if (this.draggedItem === draggedOverItem) {
      return
    }
    // filter out the currently dragged item
    let items = this.state.items.filter((item) => item !== this.draggedItem)
    // add the dragged item after the dragged over item
    items.splice(index, 0, this.draggedItem)
    this.setState({ items })
  }

  /**
   * Handler for onDragStart
   * @param {*} e
   * @param {Number} index
   */
  handleDragStart(e, index) {
    // console.log({ index }, this.state.items[index]);
    this.draggedItem = this.state.items[index]
    e.dataTransfer.effectAllowed = 'move'
    e.target.style.opacity = 1
    e.target.style.display = 'flex'
    e.dataTransfer.setData('text/html', e.target)
    e.dataTransfer.setDragImage(e.target, 0, 0)
  }

  /**
   * Handler for onDragEnd
   * @param {*} e
   */
  handleDragEnd(e) {
    this.draggedItem = null
  }

  /**
   * Handler for onDrop
   * @param {*} e
   */
  handleDrop(e) {
    e.preventDefault()
    const id = e.dataTransfer.getData('item_id')

    const card = document.getElementById(id)
    card.style.display = 'block'
    e.target.appendChild(card)
  }

  render() {
    return (
      <div className='list_container'>
        <ul className='list_ul'>
          {this.state.items.map((item, i) => {
            return (
              <li
                key={i}
                onDrop={(e) => this.handleDrop(e, i)}
                onDragOver={(e) => this.handleDragOver(e, i)}
              >
                <div
                  draggable
                  onDragOver={(e) => this.handleDragOver(e, i)}
                  onDragStart={(e) => this.handleDragStart(e, i)}
                  onDragEnd={(_) => this.handleDragEnd(_)}
                  className='list_content'
                >
                  {item}
                </div>
              </li>
            )
          })}
          <li className='hidden' text={'Add text'}>
            Add Items
          </li>
        </ul>
      </div>
    )
  }
}
