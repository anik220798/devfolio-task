/**
 * @fileoverview Implementation of drop wrapper for independent implementation
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React from 'react'

//------------------------------------------------------------------------------
// Public Components
//------------------------------------------------------------------------------

/**
 * Wrapper for drop behavior
 */
export default class Droppable extends React.Component {
  /**
   * handler for onDrop
   * params {*} e
   */
  drop = (e) => {
    e.preventDefault()
    const transfer = e.dataTransfer.getData('transfer')
    const card = document.getElementById(transfer)
    // card.style.display = "block";
    e.target.appendChild(card)
  }

  render() {
    return (
      <div
        id={this.props.id}
        onDrop={this.drop}
        onDragOver={(e) => e.preventDefault()}
      >
        {this.props.children}
      </div>
    )
  }
}
