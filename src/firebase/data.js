/**
 * @fileoverview Firebase datas and config
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

// import firebase from 'firebase/app'

//------------------------------------------------------------------------------
// Data
//------------------------------------------------------------------------------

const API_KEY = 'AIzaSyC1NiJNV7j0hOCm7xNkp3D2iBHB-mQD42U',
  AUTH_DOMAIN = 'devfolio-task-f9862.firebaseapp.com',
  DATABASE_URL = 'https://devfolio-task-f9862.firebaseio.com',
  PROJECT_ID = 'devfolio-task-f9862',
  STORAGE_BUCKET = 'devfolio-task-f9862.appspot.com',
  MESSAGING_SENDER_ID = '434146000737',
  APP_ID = '1:434146000737:web:8ddbccd84bd3fcec084f20',
  MEASUREMENT_ID = 'G-TV8B06SL1J'
export const SUCCESS = 'SUCCESS'
export const FAILED = 'FAILED'

//------------------------------------------------------------------------------
// Helper/FirebaseInitializer
//------------------------------------------------------------------------------

export const firebaseCredentials = {
  apiKey: API_KEY,
  authDomain: AUTH_DOMAIN,
  databaseURL: DATABASE_URL,
  projectId: PROJECT_ID,
  storageBucket: STORAGE_BUCKET,
  messagingSenderId: MESSAGING_SENDER_ID,
  appId: APP_ID,
  measurementId: MEASUREMENT_ID,
}

// firebase.initializeApp(firebaseCredentials)
// export const databaseRef = firebase.database().ref('task')

// export const setItemOnDB = ({ data }) =>
//   new Promise((res, rej) => {
//     try {
//       databaseRef.push(data)
//       res({ status: SUCCESS })
//     } catch (error) {
//       rej({ status: FAILED, error })
//     }
//   })

// export const getItemsFromDB = () => databaseRef.once('value')
