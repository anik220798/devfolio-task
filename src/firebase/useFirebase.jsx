/**
 * @fileoverview Firbase hook
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React, { useEffect, useState } from 'react'
import * as firebase from 'firebase/app'
import { getItemsFromDB, setItemOnDB } from './data'

//------------------------------------------------------------------------------
// Public hook
//------------------------------------------------------------------------------

export default useFirebase = ({ data }) => {
  const [task, setTask] = useState(() => {
    let task
    getItemsFromDB
      .then((res) => {
        task = res && res.val()
      })
      .catch((err) => {
        console.log({ err })
        task = []
      })
    return task
  })

  useEffect(() => {
    setItemOnDB({ data }).then((res) => {
      console.log('useEffect', res)
    })
  })

  return [task, setTask]
}
