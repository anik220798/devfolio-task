/**
 * @fileoverview Firbase wrapper
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React from 'react'
import firebase from 'firebase/app'
import 'firebase/database'
import { firebaseCredentials, SUCCESS, FAILED } from './data'

//------------------------------------------------------------------------------
// Public Component
//------------------------------------------------------------------------------

export const withFirebase = (Component) => {
  return class FirebaseWrap extends React.Component {
    constructor(props) {
      super(props)
      this.state = {}
      if (!firebase.apps.length) {
        firebase.initializeApp(firebaseCredentials)
      }

      this.db = firebase.database()
      this.addTask = this.addTask.bind(this)
      this.getTasks = this.getTasks.bind(this)
      this.deleteTask = this.deleteTask.bind(this)
    }

    /**
     * add task to firebase database
     * @param {string} data
     */
    addTask(data) {
      return new Promise(async (res, rej) => {
        try {
          const l = this.db.ref('tasks').push(data)
          await l.set({ name: data })
          res({ status: SUCCESS })
        } catch (error) {
          rej({ status: FAILED, error })
        }
      })

      // Need to call this.setState() so that it will call componentDidUpdate to update the state
    }

    /**
     * get all tasks from firebase database
     * @returns {Promise} tasks
     */
    getTasks() {
      return new Promise(async (res, rej) => {
        let tasks = []
        try {
          await this.db.ref('tasks').once('value', (ss) => {
            ss.forEach((data) => {
              tasks.push({ ref: data.key, name: data.val().name })
            })
          })
          res(tasks)
        } catch (error) {
          rej(error)
        }
      })
    }

    deleteTask(ref) {
      return new Promise(async (res, rej) => {
        try {
          await this.db.ref('tasks').child(ref).remove()
          res({ status: SUCCESS })
        } catch (error) {
          rej({ status: FAILED, error })
        }
      })
    }

    componentDidMount() {}

    componentDidUpdate() {}

    render() {
      return (
        <Component
          deleteTask={this.deleteTask}
          getTasks={this.getTasks}
          addTask={this.addTask}
        />
      )
    }
  }
}
