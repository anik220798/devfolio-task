/**
 * @fileoverview HOC for listening to mouse out
 * @author Aniketh Saha
 */

//------------------------------------------------------------------------------
// Requirements
//------------------------------------------------------------------------------

import React from 'react'

//------------------------------------------------------------------------------
// Public Component
//------------------------------------------------------------------------------

export default (Component) => {
  return class MouseOut extends React.Component {
    constructor(props) {
      super(props)
      this.handleClickOutside = this.handleClickOutside.bind(this)
      this.setWrapperRef = this.setWrapperRef.bind(this)
    }
    componentDidMount() {
      document.addEventListener('mousedown', this.handleClickOutside)
    }
    componentWillUnmount() {
      document.removeEventListener('mousedown', this.handleClickOutside)
    }

    setWrapperRef(node) {
      this.wrapperRef = node
    }

    handleClickOutside(event) {
      if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
        this.clearTagsResult()
      }
    }

    render() {
      return <Component setWrapperRef={this.setWrapperRef} />
    }
  }
}
